package com.udemy.sfg.s06httpbasicauth.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class WhitelistControllerTest extends BaseTests {

    // white list api for all HTTP method - start
    @Test
    void publicTest() throws Exception{
        mockMvc.perform(get("/whitelist/public"))
                .andExpect(status().isOk());
    }

    // white list api for all HTTP method - end

    // white list api for all HTTP GET - start
    @Test
    void getTest() throws Exception{
        mockMvc.perform(get("/whitelist/get"))
                .andExpect(status().isOk());
    }

    @Test
    void postTest() throws Exception{
        mockMvc.perform(post("/whitelist/post"))
                .andExpect(status().is4xxClientError());
    }

    // white list api for all HTTP GET - end

    // white list api for path variable matcher - start

    @Test
    void pathVariableAPITest() throws Exception{
        mockMvc.perform(get("/whitelist/1234"))
                .andExpect(status().isOk());
    }
    // white list api for path variable matcher - end
}
