package com.udemy.sfg.s06httpbasicauth.configs;

import com.udemy.sfg.s06httpbasicauth.factories.XyzPasswordEncoderFactories;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // requests.antMatchers(..) is used for whitelisting APIs
        // order of ant matcher is important
        // we can place requests.antMatchers(..) after requests.anyRequest()
        // it will give RTE as java.lang.IllegalStateException: Can't configure antMatchers after anyRequest
        http.authorizeRequests((requests) -> {
            requests
                    // this will whitelist api for all Http methods
                    .antMatchers("/whitelist/public", "/").permitAll()

                    // this will whitelist api for ONLY Http GET
                    .antMatchers(HttpMethod.GET, "/whitelist/get").permitAll()

                    // this will whitelist api for ONLY Http GET, syntax of string matches with @RequestMapping
                    // for such syntax of string we use mvcMatchers(..) in place of antMatchers(..)
                    .mvcMatchers(HttpMethod.GET, "/whitelist/{path}").permitAll()
            ;
        });
        http.authorizeRequests((requests) -> {
            ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)requests.anyRequest()).authenticated();
        });

        http.formLogin();
        http.httpBasic();
    }

    // this has been commented as we are using configure(AuthenticationManagerBuilder) i.e Fluent way
    // to create users
    /*
    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails admin = User.withDefaultPasswordEncoder()
                .username("admin")
                .password("admin")
                .roles("ADMIN")
                .build();

        UserDetails user = User.withDefaultPasswordEncoder()
                .username("inmemuser")
                .password("inmemuserpass")
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(admin,user);
    }

     */

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("noopuser")
                //.password("noopuser") // commented since we are not using NoOpPasswordEncoder
                //.password(passwordEncoder().encode("noopuser")) // commented since we using Delegating password encoder
                                                                  // if we are using speicific encoder like ldp, sha256, bcrypt
                                                                  // then this line shouldn't be commented
                                                                  // refer StudentControllerTests.testHttpBasicInMemAuthNoOpEncodedCredentialUser()
                .password("{noop}noopuser")
                .roles("NOOPUSER")
                .and()
                    .withUser("ldapuser")
                    //.password("ldapuser") // commented since we are not using NoOpPasswordEncoder
                    //.password(passwordEncoder().encode("ldapuser")) // commented since we using Delegating password encoder
                                                                      // if we are using speicific encoder like ldp, sha256, bcrypt
                                                                      // then this line shouldn't be commented
                                                                      // refer StudentControllerTests.testHttpBasicInMemAuthLdapEncodedCredentialUser()
                    .password("{ldap}{SSHA}ZvonpN7PIjlNWtPl/TuLNYfhd8mV9+i+aAj46g==") // ldap encoded string for ldapuser as password
                    .roles("LDAPUSER")
                .and()
                    .withUser("sha256user")
                    //.password(passwordEncoder().encode("sha256user")) // commented since we using Delegating password encoder
                                                                        // if we are using speicific encoder like ldp, sha256, bcrypt
                                                                        // then this line shouldn't be commented
                                                                        // refer StudentControllerTests.testHttpBasicInMemAuthSha256EncodedCredentialUser()
                    .password("{sha256}7cdfefb9b4fc94fd6cb6a7702cac0402addbdbc3bad333a31db658b932c76d688eb5e9f5b6ce9b60")  // sha256 encoded string for sha256user as password
                    .roles("SHA256USER")
                .and()
                    .withUser("bcryptuser")
                    //.password(passwordEncoder().encode("bcryptuser")) // commented since we using Delegating password encoder
                                                                        // if we are using speicific encoder like ldp, sha256, bcrypt
                                                                        // then this line shouldn't be commented
                                                                        // refer StudentControllerTests.testHttpBasicInMemAuthBCryptEncodedCredentialUser()
                    .password("{bcrypt}$2a$10$xFOsfgTS7Ggx2b0fQiS0c.2XB3uX4TLUtSY1C2NhkrtxIebXovqkC")  // BCrypt encoded string for bcryptuser as password
                    .roles("BCRYPTUSER")
                ;

        // this will keep all the user and add this user to the list
        auth.inMemoryAuthentication()
                .withUser("bcrypt15user")
                //.password(passwordEncoder().encode("bcrypt15user")) // commented since we using Delegating password encoder
                // if we are using speicific encoder like ldp, sha256, bcrypt
                // then this line shouldn't be commented
                // refer StudentControllerTests.testHttpBasicInMemAuthBCrypt15EncodedCredentialUser()
                .password("{bcrypt15}$2a$15$6msZ1FXTRL06E.XL24DY7OPJHHCx5/RBqQlU2M7fCcUMHYU9.1aVi")  // BCrypt15 encoded string for bcrypt15user as password
                .roles("BCRYPT15USER");
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        // return NoOpPasswordEncoder.getInstance();
        // return new LdapShaPasswordEncoder();
        // return new StandardPasswordEncoder();
        // return new BCryptPasswordEncoder();
        //return PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return XyzPasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}
