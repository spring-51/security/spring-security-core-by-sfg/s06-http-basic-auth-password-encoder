package com.udemy.sfg.s06httpbasicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S06HttpBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(S06HttpBasicAuthApplication.class, args);
    }

}
