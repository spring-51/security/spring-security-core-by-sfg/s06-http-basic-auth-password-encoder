# Http Basic Auth Java Configuration

## References
```ref
1. https://docs.spring.io/spring-security/site/docs/current/reference/html5
```

L32 - Password encoding
```
1. refer ../L32-PasswordEncoding.pdf
```

L33 - MD5 Hash And Password Salt
```
[HASH]

1. We can convert password string to Hash, its unidirectional.
i.e. we can't reverse engineer password string from hash.
 - there are multiple ways oh hashing, MD5 is one of then
 - problem with MD5 is that it always generate same hash for a String
 - this makes MD5 LESS SECURE

[MD5 HASH]

2. To convert String to hash we use DigestUtils 
  - DigestUtils.md5DigestAsHex(PASSWORD.getBytes())
    -- return hash value of string
    -- converting password to hash and saving to db is not secure, since 
       for a String generated hash value is always same
  - refer org.springframework.util.DigestUtils.PassEncodingTests -> hashingExampleTest()

[SALT]
3. Salt means we add additional String as prefix or postfix or both.
then we generate hash of that String.
eg . String password = "password";
     String saltedPassword = "some-prefix"+password+"some-postfix";
     DigestUtils.md5DigestAsHex(saltedPassword.getBytes())
     
  -- it's also not ideal, but it more secure than Hash only
  -- org.springframework.util.DigestUtils.PassEncodingTests -> saltingExampleTest()

Note
- MD5 Hash is LESS SECURE, hence its not recommened for encoding in modern day apps. 

```

L34 - NoOp password encoder
```
1. Noop password encoder does not encode the password, it just return the same plain string whatever we pass.
i.e.  "password" ---{noop}--> "password"

  - refer org.springframework.util.DigestUtils.PassEncodingTests -> noopPasswordEncoderExampleTest()

2. Primary purpose of NoOp pass encoder is testing spring securitty and demo purposes only.
```

L35 - LDAP password encoder
```
1. As we were disuccing MD5 Dash encoding during L33, we encounter a problem
that hashed string is always same for a string.
THis makes MD5 hashed password a lilltle easy to crack.

2. LDAP password encoding overcomes above limitaion.
Here for one password String we will get different hash values.
i.e
"password"  -- ldap encoding --> {SSHA}d2cmTNIkpUeUZ967duAm0Z3XzyZkeJUsSX6rKw==
                             --> {SSHA}XuSkNzVk5SfR9fJ+CENpM9OyfDMohU97xrDOXg==
                             --> {SSHA}P9KG0ZgWwBCyrbqcvzjL35yn+A806MYx+pQlwg==
                                 ...
                                 ...
  - refer org.springframework.util.DigestUtils.PassEncodingTests -> ldapPasswordEncoderExampleTest()
  
3. Internally Ldap encoding using random salted String, that's why we get different encoded string 
for same password string.

4. Spring security internally to validate password 
compares password String and ldap encoded String using ldapEnocoder match(..) method.
i.e.
  PasswordEncoder ldap = new LdapShaPasswordEncoder();
  ldap.matches(PASSWORD, ldap.encode(PASSWORD))
  - order is important 
    -- fisrt args  string should be plain password string and 
    -- second args string should be encode string( it generally comes from db)
  ldap.matches(PASSWORD, PASSWORD)
  - ldap encoder also matches plain string password
  
  - refer org.springframework.util.DigestUtils.PassEncodingTests -> ldapPasswordEncoderExampleTest()
```

L36 - SHA 256 password encoder
```
1. It was previously by default encoding in Spring, now it has been replaced by BCrypt encoder.
2.Internal working of ldap and sha256 is almost same, sha26 also use rand salting for encoding.  
Similer to Ldap password encoder SHA 256 also generate different encoded string for same password string
i.e
"password"  -- sha 256 encoding --> 9d63e0987f853715e61b431cf3fbab7f2ab494b279931ea902bfc8bfc84523b56095586cc04c54af
                                --> f6716f8a0372984af91627680e82706654e5143827aa411b1a93bb034278d8d6a2639294cadf237b
                                --> b358c0fd64d3dfa321babfd7334d00f95bb3ba9e56a9d52e80951acd4587c712658f5c62519e4c64
                                         ...
                                         ...

3. Only difference is with ldap we can comre plain text password
i.e.
new LdapShaPasswordEncoder().matches(PASSWORD, new LdapShaPasswordEncoder().encode(PASSWORD)) ------ return true
new LdapShaPasswordEncoder().matches(PASSWORD, PASSWORD) ------------------------------------------- RETURN TRUE
  - refer org.springframework.util.DigestUtils.PassEncodingTests -> ldapPasswordEncoderExampleTest()
  
but incase of SHA 256 it return false.

new StandardPasswordEncoder().matches(PASSWORD, new StandardPasswordEncoder().encode(PASSWORD)) ------ return true
new StandardPasswordEncoder().matches(PASSWORD, PASSWORD) -------------------------------------------- RETURN FALSE

  - refer org.springframework.util.DigestUtils.PassEncodingTests -> sha256PasswordEncoderExampleTest()

```

L37 - BCrypt password encoder
```
1. This is bydefault encoder of spring security.
Q. if this by default, then when we don't define bean of PasswordEncoder, why spring fails 
   saying no password bean defined when we uses Flunt API to define user ?
Ans - ?

2. StringLength in BCrypt encoder
PasswordEncoder bcrypt = new BCryptPasswordEncoder(int stringlength); 
 - value of stringlength range from 4 to 21
 - default value is 10
 - more the string length value, 
   -- more the work will be done to encode the password
   -- more secure is the password
   -- more time will be taken to encode the string
   
  - refer org.springframework.util.DigestUtils.PassEncodingTests -> bcryptPasswordEncoderExampleTest()

```


L38 - Delegating password encoder
```
1. It's a new feature introduced in Spring 5.
2. Its useful when we have stored password using different encoding.
i.e for some user(s) to encoded using ldap password encoder
    for some user(s) to encoded using SHA256 password encoder
    for some user(s) to encoded using Bcrypt password encoder

3.  To make delegating password encoder work we need to prefix encoded password string with an "encodingId"
  - encodingId - a unique identifier for each type of password encoder.
  - below is the list of "encodingId" assocaited to password encoder
        String encodingId = "bcrypt";
        Map<String, PasswordEncoder> encoders = new HashMap();
        encoders.put(encodingId, new BCryptPasswordEncoder());
        encoders.put("ldap", new LdapShaPasswordEncoder());
        encoders.put("MD4", new Md4PasswordEncoder());
        encoders.put("MD5", new MessageDigestPasswordEncoder("MD5"));
        encoders.put("noop", NoOpPasswordEncoder.getInstance());
        encoders.put("pbkdf2", new Pbkdf2PasswordEncoder());
        encoders.put("scrypt", new SCryptPasswordEncoder());
        encoders.put("SHA-1", new MessageDigestPasswordEncoder("SHA-1"));
        encoders.put("SHA-256", new MessageDigestPasswordEncoder("SHA-256"));
        encoders.put("sha256", new StandardPasswordEncoder());
        encoders.put("argon2", new Argon2PasswordEncoder());
4. To save passord in in-mem or db we need to prefix "encodingId" to encoded password.
  - refer com.udemy.sfg.s05httpbasicauth.configs.SecurityConfig -> configure(AuthenticationManagerBuilder)
  
  @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("noopuser")
                .password("{noop}noopuser")
                .roles("NOOPUSER")
                .and()
                    .withUser("ldapuser")
                    .password("{ldap}{SSHA}ZvonpN7PIjlNWtPl/TuLNYfhd8mV9+i+aAj46g==") // ldap encoded string for ldapuser as password
                    .roles("LDAPUSER")
                .and()
                    .withUser("sha256user")
                    .password("{sha256}7cdfefb9b4fc94fd6cb6a7702cac0402addbdbc3bad333a31db658b932c76d688eb5e9f5b6ce9b60")  // sha256 encoded string for sha256user as password
                    .roles("SHA256USER")
                .and()
                    .withUser("bcryptuser")
                    .password("{bcrypt}$2a$10$xFOsfgTS7Ggx2b0fQiS0c.2XB3uX4TLUtSY1C2NhkrtxIebXovqkC")  // BCrypt encoded string for bcryptuser as password
                    .roles("BCYPTUSER")
                ;
    }

  - refer JUnitTests of StudentControllerTests

```

L39 - Custom Delegating password encoder
```
1. we can use spring provided Defegatin passpword encoder without any problem, but 
if we want to control number of encoder that our app suppport we can use custom elegating password encoder.

2. Let's say we want our app to supprt only 4 encoding out of n spring provided encoder then.

  Step 1 - Create Own Delegating factory class as XyzPasswordEncoderFactories
  Step 2 - Restrict init of XyzPasswordEncoderFactories i.e mark the constructor private
  Step 3 - Copy body of PasswordEncoderFactories.createDelegatingPasswordEncoder and
           paste under  XyzPasswordEncoderFactories.createDelegatingPasswordEncoder.
  Step 4 - Remove extra entries of password encoder from the map.
  Step 5 - Modify Password bean creation , use XyzPasswordEncoderFactories in place of PasswordEncoderFactories.
```